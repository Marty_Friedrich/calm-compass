extends Area3D
class_name TalkArea

@export var dialog:DialogueResource
@export var conversation:String
@export var disableAfterwards:bool = true

@export var next:TalkArea



func _ready():
	body_entered.connect(entered)

func entered(body:Node3D):
	if !body.is_in_group("Player"):
		return
	get_tree().get_first_node_in_group("Player").disable_ray()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	get_tree().get_first_node_in_group("Player").immobile = true
	DialogueManager.show_example_dialogue_balloon(dialog, conversation)
	await DialogueManager.dialogue_ended
	get_tree().get_first_node_in_group("Player").immobile = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	if disableAfterwards == true:
		monitoring = false
	if next:
		next.monitoring = true
	await get_tree().create_timer(0.1).timeout
	get_tree().get_first_node_in_group("Player").enable_ray()
