@tool
extends Node
class_name FloatingComp

@export var preview:bool = false:
	set(new):
		preview = new
		if Engine.is_editor_hint():
			set_process(new)
			initPos = get_parent().position
		

@export var amplitude:float = 1.0

@onready var initPos:Vector3 = get_parent().position

@export var initFloat:bool = true

@export var speed:float = 1.0

var floating:bool = true

func _ready():
	if Engine.is_editor_hint():
		set_process(preview)
	

func set_floating(state:bool):
	floating = state

func set_point(point:Vector3):
	initPos = point


var time:float = 0.0

func _process(delta):
	if floating:
		get_parent().position.y = initPos.y + sin(time) * amplitude
	time += delta * speed
