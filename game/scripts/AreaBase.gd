extends Area3D
class_name AreaBase

@export var areaName:String

@export var env:Environment
@export var lightColor:Color
@export var transitionTime:float = 4.0

@export var lightEnergy:float = 1.0

@export var parameters:Array[String]

@export var music:AudioStream

var initEnv:Environment

func _ready():
	body_entered.connect(entered)
	body_exited.connect(exited)
	if %Env:
		initEnv = %Env.environment.duplicate()

func entered(body:Node3D):
	if !body.is_in_group("Player"):
		return
	print("this guy entered! " + body.name)
	if music:
		await get_tree().create_timer(0.1).timeout
		%Music.switch_to_song(music)
	var TW:Tween = create_tween()
	TW.set_parallel(true)
	for i in parameters:
		TW.tween_property(%Env, "environment:" + i, env.get(i), transitionTime)
	if lightColor:
		TW.tween_property(%Sun, "light_color", lightColor, transitionTime)
	TW.tween_property(%Sun, "light_energy", lightEnergy, transitionTime)
	if areaName != "":
		%AreaName.text = areaName
		%AreaName.modulate = Color.WHITE
		TW.tween_property(%AreaName, "self_modulate", Color.WHITE, 1.3)
		await TW.finished
		await get_tree().create_timer(3.0).timeout
		create_tween().tween_property(%AreaName, "self_modulate", Color.TRANSPARENT, 1.3)

func exited(body:Node3D):
	if !body.is_in_group("Player"):
		return
	if %Music:
		%Music.switch_music_back()
	var TW:Tween = create_tween()
	TW.set_parallel(true)
	for i in parameters:
		TW.tween_property(%Env, "environment:" + i, initEnv.get(i), transitionTime)
	TW.tween_property(%Sun, "light_color", Color.WHITE, transitionTime)
	TW.tween_property(%Sun, "light_energy", 1.0, transitionTime)
