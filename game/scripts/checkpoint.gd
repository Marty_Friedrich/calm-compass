extends Node3D

@export var sky:Sky

@export var skyVariables:Array[String]

@export var transitionTime:float = 2.0

@export var isStart:bool = false

@onready var between:Area3D = $Inbetween

var initEnv:Environment = preload("res://gispaats_home/gispaats_home.tres")

@onready var insideHidden:Array[MeshInstance3D] = [$Entrance/Tube, $Entrance/BackDoor, $Entrance/BackFrame]


@onready var music:AudioStream = preload("res://sound/music/Crystal Palace.ogg")


func _ready():
	sky = sky.duplicate(true)
	for i in get_children(true):
		if i is CollisionShape3D:
			i.disabled = true

	if SaveSystem.has(G.temp["SCENE_NAME"] + "_CHECKPOINT"):
		if round(str_to_var(SaveSystem.get_var(G.temp["SCENE_NAME"] + "_CHECKPOINT"))) == round(%Spawn.global_position):
			get_tree().get_first_node_in_group("Player").global_position = %Spawn.global_position
			get_tree().get_first_node_in_group("Player").get_node("Head").look_at(%LookAt.global_position)
			get_viewport().get_camera_3d().set_cull_mask_value(1, false)
			get_viewport().get_camera_3d().set_cull_mask_value(2, true)


var isInside:bool = false



func transition_to_inside(state:bool):
	var TW:Tween = create_tween()
	TW.set_parallel(true)
	if state == true:
		for i in skyVariables:
			TW.tween_property(get_world_3d().environment, "sky:sky_material:" + i, sky.sky_material.get(i), transitionTime)
		SoundManager.play_music(music, 3.0)
		TW.tween_property(get_world_3d().environment, "fog_density", 0.0, transitionTime)
		TW.tween_property(get_tree().get_first_node_in_group("Sun"), "light_energy", 0.0, transitionTime)
		await get_tree().create_timer(0.5).timeout
		get_viewport().get_camera_3d().set_cull_mask_value(1, false)
		get_viewport().get_camera_3d().set_cull_mask_value(2, true)
	else:
		for i in skyVariables:
			TW.tween_property(get_world_3d().environment, "sky:sky_material:" + i, initEnv.sky.sky_material.get(i), transitionTime)
		if isStart == false:
			SoundManager.play_music(G.temp["DEFAULT_MUSIC"], 3.0)
		TW.tween_property(get_world_3d().environment, "fog_density", initEnv.fog_density, transitionTime)
		TW.tween_property(get_tree().get_first_node_in_group("Sun"), "light_energy", 1.0, transitionTime)
		await get_tree().create_timer(0.5).timeout
		get_viewport().get_camera_3d().set_cull_mask_value(1, true)
		get_viewport().get_camera_3d().set_cull_mask_value(2, false)
	await TW.finished


func set_collision(player:CharacterBody3D, inside:bool):
	player.set_collision_layer_value(1, !inside)
	player.set_collision_layer_value(10, inside)
	player.set_collision_mask_value(1, !inside)
	player.set_collision_mask_value(10, inside)


func stairs_entered(body):
	if !body.is_in_group("Player"):
		return
	for i in between.get_overlapping_bodies():
		if i.is_in_group("Player"):
			return
	$Anim.play("open")
	set_collision(body, true)
	for i in get_children(true):
		if i is CollisionShape3D:
			i.disabled = false
	for i in insideHidden:
		i.visible = true
		


func stairs_exited(body):
	if !body.is_in_group("Player"):
		return
	var isValid:bool = false
	for i in between.get_overlapping_bodies():
		if i.is_in_group("Player"):
			isValid = true
			break
	if isValid == false:
		for i in get_children(true):
			if i is CollisionShape3D:
				i.disabled = false
		$Anim.play_backwards("open")
		set_collision(body, false)
		await $Anim.animation_finished
		for i in insideHidden:
			i.visible = false


func tube_entered(body):
	if !body.is_in_group("Player"):
		return
	if isInside == false:
		$Anim.play("transition")
		transition_to_inside(true)
	else:
		$Anim.play_backwards("transition")
		transition_to_inside(false)
	isInside = !isInside



func assign_shader(value:Variant, key:String):
	%Effect.mesh.material.set_shader_parameter(key, value)
	pass


func checkpoint_entered(body):
	if !body.is_in_group("Player") or $InsideRoom/StartingInit.timer > 0 or isStart == true:
		return
	SaveSystem.set_var(G.temp["SCENE_NAME"] + "_CHECKPOINT", var_to_str(%Spawn.global_position))
	SaveSystem.save()
	$Control/AnimUI.stop()
	if !$Anim.is_playing():
		$Anim.play("checkpoint")
	$Control/AnimUI.play("activate")
