@tool
extends Node
class_name GameInitializer

## Name of the level. Must not be the actual scene name
@export var sceneName:String
## Time for transitioning into the gameplay
@export var transitionTime:float = 3.0
## Initial color of transition rectangle
@export_color_no_alpha var initColor:Color = Color.WHITE

@export var setViewport:ViewportTexture:
	set(new):
		if !Engine.is_editor_hint():
			return
		setViewport = new
		for i in get_tree().get_nodes_in_group("Shower"):
			i.material.set_shader_parameter("insideRoom", new)
@export var initEnv:Environment


var scatterAmount:int = 0

var scattersLeft:int = 0

func _init():
	G.temp["SCENE_NAME"] = sceneName
	pass

func _ready():
	if Engine.is_editor_hint():
		return
	if initEnv:
		get_parent().get_world_3d().environment = initEnv
	if get_node_or_null("TransRect"):
		$TransRect.color = initColor
	
	for i in get_tree().get_nodes_in_group("Shower"):
		i.material.set_shader_parameter("insideRoom", setViewport)
	
	for i in get_tree().get_nodes_in_group("HeavyScatter"):
		scatterAmount += 1
		scattersLeft += 1
		i.build_completed.connect(scatter_finished)

signal all_done

func scatter_finished():
	scattersLeft -= 1
	if scattersLeft == 0:
		start_game()


func start_game():
	if SaveSystem.has(sceneName + "_player_position"):
		get_tree().get_first_node_in_group("Player").global_position = SaveSystem.get_var(sceneName + "_player_position")
	
	if get_node_or_null("TransRect"):
		var TW:Tween = create_tween()
		TW.tween_property($TransRect, "color", Color.TRANSPARENT, transitionTime)
		await TW.finished
		$TransRect.visible = false
