extends Area3D
class_name ObjectiveArea

@export var objectiveMessage:String


func entered(body:Node3D):
	if !body.is_in_group("Player"):
		return
	G.set_objective(objectiveMessage)
	queue_free()
