extends Node
class_name Music

@export var defaultMusic:AudioStream


@export var autoPlay:bool = true

func _ready():
	G.temp["DEFAULT_MUSIC"] = defaultMusic
	if autoPlay == true and defaultMusic:
		SoundManager.play_music(defaultMusic)
	if %Wind:
		%Wind.volume_db = -80
		create_tween().tween_property(%Wind, "volume_db", 0.0, 6.0)

func switch_to_song(new:AudioStream):
	SoundManager.play_music(new, 3.0)

func switch_music_back():
	SoundManager.play_music(defaultMusic, 5.0)
