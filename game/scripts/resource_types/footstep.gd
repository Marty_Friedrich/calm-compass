extends Resource
class_name Footstep

@export var floorName:String
@export var sounds:Array[AudioStream]


var lastSound:AudioStream

func pick_sound() -> AudioStream:
	var random:AudioStream = sounds.pick_random()
	
	while random == lastSound and sounds.size() != 1:
		randomize()
		random = sounds.pick_random()
	lastSound = random
	return random

