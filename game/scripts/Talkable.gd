extends StaticBody3D
class_name Talkable


@export var dialog:DialogueResource
@export var conversation:String

@export var alternativeSprite:Texture2D


func _ready():
	if G.temp.has("SCENE_NAME") and SaveSystem.has(G.temp["SCENE_NAME"] + "_G_LEFT") and SaveSystem.get_var(G.temp["SCENE_NAME"] + "_G_LEFT") == true:
		queue_free()
	if get_node_or_null("AnimationPlayer"):
		$AnimationPlayer.play("default")
	if G.temp.has("SECRET") and G.temp["SECRET"] == true and $Sprite3D:
		$Sprite3D.texture = alternativeSprite
	

func interact():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	get_tree().get_first_node_in_group("Player").immobile = true
	get_tree().get_first_node_in_group("Player").disable_ray()
	DialogueManager.show_example_dialogue_balloon(dialog, conversation)
	await DialogueManager.dialogue_ended
	if get_tree():
		get_tree().get_first_node_in_group("Player").immobile = false
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		get_tree().get_first_node_in_group("Player").enable_ray()
