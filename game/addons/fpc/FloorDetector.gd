extends RayCast3D


@export var footSteps:Array[Footstep]

func play_footstep():
	if !get_collider() or get_parent().dir == Vector2.ZERO:
		return
	var found:bool = false
	for i in footSteps:
		if get_collider().is_in_group(i.floorName):
			$Step.stream = i.pick_sound()
			found = true
	$Step.play()
