extends Node


var temp:Dictionary

var blender:ColorRect

func _ready():
	set_process(false)
	blender = ColorRect.new()
	blender.set_anchors_preset(Control.PRESET_FULL_RECT)
	blender.color = Color.TRANSPARENT
	blender.z_index = 10
	add_child(blender)
	

func start_quiz():
	get_tree().get_first_node_in_group("Player").disable_ray()
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	SoundManager.play_music(load("res://sound/music/No More Magic.mp3"))
	DialogueManager.show_example_dialogue_balloon(load("res://areas/gisperts_forest/gispaat.dialogue"), "quiz")
	await DialogueManager.dialogue_ended
	get_tree().get_first_node_in_group("Player").enable_ray()
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func congratulations():
	SoundManager.play_music(load("res://sound/sfx/congrats.ogg"), 0.3)
	get_tree().get_first_node_in_group("Player").jump_velocity = 7.0
	G.temp["quiz_solved"] = true

func to_moon():
	get_tree().root.get_node("Game/Anim").play("to_moon")
	await get_tree().root.get_node("Game/Anim").animation_finished
	get_tree().change_scene_to_file("res://addons/proton_scatter/demos/assets/moon.tscn")
	

func start_epilog():
	var TW:Tween = create_tween()
	blender.visible = true
	TW.tween_property(blender, "color", Color.WHITE, 3.0)
	await TW.step_finished
	TW.stop()
	get_tree().change_scene_to_file("res://gispaats_home.tscn")
	await get_tree().create_timer(0.3)
	TW.tween_property(blender, "color", Color.TRANSPARENT, 5.0)
	await TW.finished
	blender.visible = false
	get_tree().change_scene_to_file("res://gispaats_home.tscn")



func raise_gispaat():
	get_tree().create_tween().tween_property(get_tree().get_first_node_in_group("FirstG"), "position:y", get_tree().get_first_node_in_group("FirstG").position.y + 10.0, 4.0)
	get_tree().create_timer(4.0).timeout.connect(get_tree().get_first_node_in_group("FirstG").queue_free)
	SaveSystem.set_var(temp["SCENE_NAME"] + "_G_LEFT", true)


func got_all_apples(speed:int):
	get_tree().get_first_node_in_group("Player").sprint_speed = speed
	get_tree().get_first_node_in_group("Player").base_speed = speed * 0.5


func event():
	get_tree().root.get_node("Game/Anim").play("event")
	await get_tree().root.get_node("Game/Anim").animation_finished
	

var timer:float = 120


func world_dead():
	set_process(false)
	timer = 120.0
	get_tree().get_first_node_in_group("Anim").play("world_dead")
	await get_tree().get_first_node_in_group("Anim").animation_finished
	SoundManager.stop_music(1.0)
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	get_tree().call_deferred("change_scene_to_file", "res://game_over.tscn")

func _process(delta):
	timer -= delta
	if timer <= 0:
		world_dead()
	get_tree().get_first_node_in_group("Timer").text = str(timer) + " seconds remaining"


func win_game():
	get_tree().change_scene_to_file("res://credits.tscn")

func gispaat_crash():
	get_tree().root.get_node("Moon/Anim").play("crash")
	await get_tree().create_timer(1.5)
	DialogueManager.show_example_dialogue_balloon(load("res://areas/gisperts_forest/gispaat.dialogue"), "crash")
	await DialogueManager.dialogue_ended
	get_tree().root.get_node("Moon/Anim/Camera3D").current = false


func switch_to_game():
	var TW:Tween = create_tween()
	blender.visible = true
	TW.tween_property(blender, "color", Color.WHITE, 3.0)
	TW.tween_property(blender, "color", Color.TRANSPARENT, 4.0)
	await TW.step_finished
	get_tree().change_scene_to_file("res://game.tscn")
