extends Node3D


@onready var orb:MeshInstance3D = $Pillar/Orb

@onready var initPos:Vector3 = orb.global_position

var time:float = 0.0

func _process(delta):
	time += delta
	orb.global_position.y = initPos.y + sin(time) * 0.2
	orb.rotation.y += delta * 0.2
