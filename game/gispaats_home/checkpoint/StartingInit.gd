extends Node


@export var insideRoom:Area3D

var timer:float = 1.0

func _ready():
	var valid:bool = false
	for i in insideRoom.get_overlapping_bodies():
		if i.is_in_group("Player"):
			valid = true
			break
	if valid == false:
		return
	for i in get_parent().get_parent().get_children():
		if i is CollisionShape3D:
			i.disabled = false
	for i in get_parent().get_parent().insideHidden:
		i.visible = true
	var anim:AnimationPlayer = get_parent().get_parent().get_node("Anim")
	anim.speed_scale = 100.0
	anim.play("transition")
	await anim.animation_finished
	anim.speed_scale = 1.0
	

func _process(delta):
	timer -= delta
	if timer < 0:
		set_process(false)



func _on_inside_room_body_entered(body):
	if !body.is_in_group("Player") or timer < 0:
		return
	for i in get_parent().get_parent().get_children():
		if i is CollisionShape3D:
			i.disabled = false
	for i in get_parent().get_parent().insideHidden:
		i.visible = true
	get_tree().create_timer(0.1).timeout.connect(set_sky)
	get_parent().get_parent().isInside = true
	get_parent().get_world_3d().environment.fog_density = 0.0
	var anim:AnimationPlayer = get_parent().get_parent().get_node("Anim")
	anim.speed_scale = 100.0
	anim.play("transition")
	await anim.animation_finished
	anim.speed_scale = 1.0
	

func set_sky():
	print("the sky top color is " + str(get_parent().get_parent().sky.sky_material.sky_top_color))
	get_parent().get_world_3d().environment.sky = get_parent().get_parent().sky.duplicate(true)
	
