extends Camera3D


func _process(delta):
	global_position = get_viewport().get_camera_3d().global_position + Vector3(0.0, 4.0, 0.0)
	global_rotation = get_viewport().get_camera_3d().global_rotation
	fov = get_viewport().get_camera_3d().fov
