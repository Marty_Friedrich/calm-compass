extends Control

@onready var music:AudioStream = preload("res://sound/music/Production_Intro_Theme.wav")


func play_intro():
	SoundManager.play_music(music, 0.5)
	$Anim.play("intro")
	await $Anim.animation_finished
	visible = false
	SoundManager.play_music(G.temp["DEFAULT_MUSIC"], 4.0)
	queue_free()
