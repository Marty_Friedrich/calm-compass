# Calm Compass
 A game a made in three days. What the game is about is for the player to find out.


### Trailer
[![](http://img.youtube.com/vi/hgm1dmchjgQ/0.jpg)](http://www.youtube.com/watch?v=hgm1dmchjgQ "Release Trailer")
### Second teaser trailer
[![](http://img.youtube.com/vi/Vt1heJVDyCI/0.jpg)](http://www.youtube.com/watch?v=Vt1heJVDyCI "Update Teaser Trailer")


I improved the performance drastically in the second day, almost doubeling the frame rate from 200 to 400 on my computer.
Now it even runs with 90 fps on my Pixel 6 phone, so there should not be any performance issues, as long as your computer is not ancient.

Have fun.
